let express =   require('express');
let cors =      require('cors');
let app =       express();


////////////////////////////////////////// config //////////////////////////////////////////

app.use(cors({origin: 'http://localhost:4200'}));


/////////////////////////////////// endpoint definitions ///////////////////////////////////

require('./routes')(app);


////////////////////////////////////////// listen //////////////////////////////////////////

let server = app.listen(process.env.PORT || 8080, function () {
    let port = server.address().port;
    console.log("Server now running on port", port);
});