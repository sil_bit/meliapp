# Meliapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Backend development server

Run `npm run start-be`. This will build and start a local backend server on `http://localhost:8080/`.

## Frontend development server

Run `npm run start-fe` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. It needs the backend server up to work correctly.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
