import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'meli-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent {
  searchText = '';

  constructor(private router: Router) {}

  goSearchResults() {
    this.router.navigate(['/items'], { queryParams: { search: this.searchText } });
  }

}
