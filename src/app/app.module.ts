import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { ItemsModule } from './items/items.module';
import { ItemListComponent } from './items/item-list/item-list.component';
import { ItemDetailsComponent } from './items/item-details/item-details.component';
import { SearchBoxComponent } from './search-box/search-box.component';


const APP_ROUTES: Routes = [
  {
    path: 'items',
    pathMatch: 'full',
    component: ItemListComponent
  },
  {
    path: 'items/:id',
    component: ItemDetailsComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    SearchBoxComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(APP_ROUTES),
    FormsModule,
    ItemsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
