import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';

import { ItemService } from '../services/item.service';
import { Item } from '../models/item.model';

@Component({
  selector: 'meli-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  items: Item[] = [];
  breadcrumb: string[] = [];
  noResultsFound = false;

  constructor(private meta: Meta, private title: Title,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private itemService: ItemService) {

    this.title.setTitle('Resultados de la Búsqueda');
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {

      const searchText = params['search'];

      if (searchText) {

        // SEO
        this.meta.updateTag(
          { name: 'description', content: 'Resultados de la búsqueda por ' + searchText + ' en Meliapp'}
        );

        this.itemService.getItems(searchText).then(response => {
          if (response) {
            this.items = response.items;
            this.breadcrumb = response.categories;
            this.noResultsFound = response.items.length === 0;
          } else {
            this.noResultsFound = true;
          }
        });

      } else {
        this.router.navigateByUrl('/');
      }
    });
  }
}

