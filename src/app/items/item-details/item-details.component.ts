import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';

import { ItemService } from '../services/item.service';
import { Item } from '../models/item.model';


@Component({
  selector: 'meli-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {

  item: Item;
  itemNotFound = false;

  constructor(private meta: Meta, private title: Title,
              private activatedRoute: ActivatedRoute,
              private itemService: ItemService) {

    this.title.setTitle('Detalles del producto');
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {

      const itemId = params['id'];
      this.itemService.getItem(itemId).then((item: Item) => {

        if (item) {
          this.itemNotFound = false;
          this.item = item;

          // SEO
          this.meta.updateTag(
            { name: 'description', content: 'Detalles del producto ' + item.title + ' en Meliapp'}
          );

        } else {
          this.itemNotFound = true;
        }
      });
    });
  }

  // todo: move to a lookup service?
  getCondition(key) {
    if (key === 'new') {
      return 'Nuevo';
    } else if (key === 'used') {
      return 'Usado';
    } else {
      return 'Desconocido';
    }
  }

  goCheckout() {
    alert('¡Gracias por su compra!'); // todo: checkout page not implemented yet
  }
}
