export class Item {
  id: string;
  title: string;
  price: {
    currency: string,
    amount: number,
    decimals: number
  };
  address: {
    state_name: string,
    city_name: string
  };
  picture: string;
  condition: string;
  free_shipping: boolean;
  description: string;
}
