import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Item } from '../models/item.model';

@Component({
  selector: 'meli-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnChanges {
  @Input() item: Item;

  constructor(private router: Router) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.item.currentValue) {
      this.item = changes.item.currentValue;
    }
  }

  goItemDetails() {
    this.router.navigate(['/items', this.item.id]);
  }
}
