import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Item } from '../models/item.model';

@Injectable()
export class ItemService {

  private _itemsUrl =     'http://localhost:8080/api/items?q=';
  private _itemInfoUrl =  'http://localhost:8080/api/items/';

  constructor(private httpClient: HttpClient) {}

  getItems(query): Promise<any> {
    return this.httpClient.get<any>(this._itemsUrl + query)
               .toPromise()
               .then(response => {
                 return {
                   items: response.items,
                   categories: response.categories
                 };
               })
               .catch(this.handleError);
  }

  getItem(id: string): Promise<void | Item> {
    return this.httpClient.get<any>(this._itemInfoUrl + id)
      .toPromise()
      .then(response => response.item as Item)
      .catch(this.handleError);
  }

  private handleError (error: any) {
    const errMsg = error.message || error.status || 'Server error';
    console.error(errMsg);
  }

}
