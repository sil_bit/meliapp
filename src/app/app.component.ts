import { Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'meli-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  constructor(meta: Meta, title: Title) {

    // SEO

    title.setTitle('Inicio');

    meta.addTags([
      { name: 'author', content: 'Silvana Rosales' },
      { name: 'description', content: 'Comprá online fácilmente con Meliapp!' }
    ]);
  }

}
