let request =   require('request');

const MAX_SEARCH_RESULTS_COUNT = 4;

module.exports = function(app) {

    // all api endpoints will start with a prefix

    let MY_API_PREFIX = '/api';


    // search items endpoint

    let ITEMS_URL = MY_API_PREFIX + '/items';
    let ML_SEARCH_URL = 'https://api.mercadolibre.com/sites/MLA/search?q=';

    app.get(ITEMS_URL, function(req, res) {
        request.get({ url: ML_SEARCH_URL.concat(req.query.q), json: true }, function(error, response, body) {

            if (!error && response.statusCode == 200) {
                res.json(formatSearchPayload(body));
            } else handleError(res, error, 'Failed to get items from source.');

        });
    });


    // get item info endpoint

    let ITEM_INFO_URL = MY_API_PREFIX + '/items/:id';
    let ML_ITEM_INFO_URL = 'https://api.mercadolibre.com/items/';

    app.get(ITEM_INFO_URL, function(req, res) {

        const item_info_url = ML_ITEM_INFO_URL.concat(req.params.id);

        request.get({ url: item_info_url, json: true }, function(error, response, body_item_info) {

            if (!error && response.statusCode == 200) {
                request.get({ url: item_info_url.concat('/description'), json: true },
                            function(error, response, body_item_desc) {

                    if (!error && response.statusCode == 200) {
                        res.json(formatItemInfoPayload(body_item_info, body_item_desc));
                    } else handleError(res, error, 'Failed to get item description from source.');

                });
            } else handleError(res, error, 'Failed to get item info from source.');

        });
    });
};


///////////////////////////////////////// helpers //////////////////////////////////////////


// Generic error handler used by all endpoints

function handleError(res, reason, message, code) {
    console.log('ERROR: ' + reason);
    res.status(code || 500).json({'error': message});
}


// input: a data response from ML search endpoint
// output: payload to be returned in the correct format in our search endpoint

function formatSearchPayload(ml_jsondata) {

    const myResponse = {
        author: {
            name: 'Silvana',
            lastname: 'Rosales'
        },
        categories: [],
        items: []
    };

    // build categories array, which is the subsequent category filters applied, aka breadcrumb

    let categoryInfo = ml_jsondata.filters.find(filterInfo => filterInfo.id === 'category');
    if (categoryInfo && categoryInfo.values.length > 0) {
        let path_from_root = categoryInfo.values[0].path_from_root;
        myResponse.categories = path_from_root.map(node => node.name);
    }

    ml_jsondata.results.slice(0, MAX_SEARCH_RESULTS_COUNT).forEach(result => {
        myResponse.items.push({
            id: result.id,
            title: result.title,
            price: {
                currency: result.currency_id,
                amount: result.price,
                decimals: 0 // todo: where is this piece of data?
            },
            address: {
                city_name: result.address.city_name,
                state_name: result.address.state_name
            },
            picture: result.thumbnail,
            condition: result.condition,
            free_shipping: result.shipping.free_shipping
        });
    });

    return myResponse;
}


// input: a data response from ML item info endpoints
// output: payload to be returned in the correct format in our item info endpoint

function formatItemInfoPayload(ml_item_id_response, ml_item_desc_response) {

    const myResponse = {
        author: {
            name: 'Silvana',
            lastname: 'Rosales'
        },
        item: {
            id: ml_item_id_response.id,
            title: ml_item_id_response.title,
            price: {
                currency: ml_item_id_response.currency_id,
                amount: ml_item_id_response.price,
                decimals: 0 // todo: where is this piece of data?
            },
            picture: ml_item_id_response.pictures.length? ml_item_id_response.pictures[0].url : '',
            condition: ml_item_id_response.condition,
            free_shipping: ml_item_id_response.shipping.free_shipping,
            sold_quantity: ml_item_id_response.sold_quantity,
            description: ml_item_desc_response.plain_text
        }
    };

    return myResponse;
}
